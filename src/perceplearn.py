import sys
import glob
import errno
from collections import defaultdict
from collections import Counter
finput = str(sys.argv[1])
foutput = str(sys.argv[2])
f2=open(foutput,'w')
w={}
wavg={}
with open(finput,'r') as f1: 
 for line in f1:
  words=line.split()
  if words[0] not in w:
   w[words[0]]=defaultdict(int)
   wavg[words[0]]=defaultdict(int)
for i in range(15):   
 with open(finput,'r') as f1:
  for line in f1:
   words=line.split()
   weights=defaultdict(int)
   x=defaultdict(int)
   for word in words[1:]:
    x[word]+=1
   for key1 in w:
    for key2 in x:
     weights[key1]+=w[key1][key2]*x[key2]
   k = max(weights,key=weights.get)
   if k != words[0]:
    for key3 in x:
     w[k][key3]-=x[key3]
     w[words[0]][key3]+=x[key3]     
 for key in w:
  a=Counter(w[key])
  b=Counter(wavg[key])
  c=a+b
  wavg[key]=dict(c)
for key in wavg:
 f2.write("***NAMEDCLASSES***")
 f2.write(" ")
 f2.write(str(key))
 f2.write("\n")
 for feature in wavg[key]:
  f2.write(str(feature))
  f2.write(" ")
  f2.write(str(wavg[key][feature]))
  f2.write("\n")

   
   
 

