import sys
import glob
import errno
from collections import defaultdict
import itertools
import codecs
finput = str(sys.argv[1])
f2=open('test1.txt','w',errors='ignore')
w={}
search = '***NAMEDCLASSES***'
label = {}
j = 0
sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')
with open(finput, 'r') as f1:
 for line in f1:
  if line.startswith(search):
   words = line.split()
   label[j] = words[1]
   w[j] = defaultdict(int)
   j = j+1
   line = next(f1)
  words = line.split()
  w[j-1][words[0]] = words[1]
for line in sys.stdin:
 if line != '\n':
  words=line.split()
  l=len(words)
  for i,w1 in enumerate(words):
   if i!=0 and i!=1:
    prevword=words[i-1]
    prevword2=words[i-2]
   elif i==0:
    prevword='BOS'
    prevword2='BOS'
   elif i==1:
    prevword=words[i-1]
    prevword2='BOS'
   if i != l-1 and i!= l-2:
    nextword=words[i+1]
    nextword2=words[i+2]
   elif i==l-1:
    nextword='EOS'
    nextword2='EOS'
   elif i==l-2:
    nextword=words[i+1]
    nextword2='EOS'       
   f2.write(str('l:'+prevword2+' '+'l:'+prevword+' '+'c:'+w1+' '+'r:'+nextword+' '+'r:'+nextword2))
   f2.write('\n')
 elif line == '\n':
  f2.write('\n')
f2.close()      
with open('test1.txt','r') as f3:
 for line in f3:
  if line != '\n':
   words=line.split()
   currword=words[2].split(':',1)
   weights1=defaultdict(int)
   if currword[1] in label.values():
    for key1 in w:
     for word in words:
      weights1[label[key1]]+=int(w[key1][word])
   
    if currword[1]=='too' or currword[1]=='to':
     if (weights1['to'])>(weights1['too']):
      sys.stdout.write('to'+' ')
     else:
      sys.stdout.write('too'+' ')
    elif currword[1]=="Too" or currword[1]=='To':
     if (weights1["To"])>(weights1['Too']):
      sys.stdout.write("To"+' ')
     else:
      sys.stdout.write('Too'+' ')   
    elif currword[1]=="you're" or currword[1]=='your':
     if (weights1["you're"])>(weights1['your']):
      sys.stdout.write("you're"+' ')
     else:
      sys.stdout.write('your'+' ') 
    elif currword[1]=="You're" or currword[1]=='Your':
     if (weights1["You're"])>(weights1['Your']):
      sys.stdout.write("You're"+' ')
     else:
      sys.stdout.write('Your'+' ')   
    elif currword[1]=="they're" or currword[1]=='their':
     if (weights1["they're"])>(weights1['their']):
      sys.stdout.write("they're"+' ')
     else:
      sys.stdout.write('their'+' ')  
    elif currword[1]=="They're" or currword[1]=='Their':
     if (weights1["They're"])>(weights1['Their']):
      sys.stdout.write("They're"+' ')
     else:
      sys.stdout.write('Their'+' ')    
    elif currword[1]=="loose" or currword[1]=='lose':
     if (weights1["loose"])>(weights1['lose']):
      sys.stdout.write("loose"+' ')
     else:
      sys.stdout.write('lose'+' ')  
    elif currword[1]=="Loose" or currword[1]=='Lose':
     if (weights1["Loose"])>(weights1['Lose']):
      sys.stdout.write("Loose"+' ')
     else:
      sys.stdout.write('Lose'+' ')    
    elif currword[1]=="it's" or currword[1]=='its':
     if (weights1["it's"])>(weights1['its']):
      sys.stdout.write("it's"+' ')
     else:
      sys.stdout.write('its'+' ')   
    elif currword[1]=="It's" or currword[1]=='Its':
     if (weights1["It's"])>(weights1['Its']):
      sys.stdout.write("It's"+' ')
     else:
      sys.stdout.write('Its'+' ')     
   else:
    sys.stdout.write(currword[1]+' ')
   if words[3]=='r:EOS': 
    sys.stdout.write('\n') 
  elif line == '\n':
   sys.stdout.write(line)
