import sys
f2=open('input.txt','w',errors='ignore')
a = ["it's","its","you're","your","they're","their","loose","lose","to","too"]
with open('training.txt','r',errors='ignore') as f1: 
 for line in f1:
  words = line.split()
  l=len(words)
  for i,w in enumerate(words):
   for j in a:
    if j == w:
     if i!=0 and i!=1:
      prevword=words[i-1]
      prevword2=words[i-2]
     elif i==0:
      prevword='BOS'
      prevword2='BOS'
     elif i==1:
      prevword=words[i-1]
      prevword2='BOS'
     if i != l-1 and i!= l-2:
      nextword=words[i+1]
      nextword2=words[i+2]
     elif i==l-1:
      nextword='EOS'
      nextword2='EOS'
     elif i==l-2:
      nextword=words[i+1]
      nextword2='EOS'       
     f2.write(str(w+' '+'l:'+prevword2+' '+'l:'+prevword+' '+'r:'+nextword+' '+'r:'+nextword2))
     f2.write('\n') 
f2.close()     
     
     