//HOW TO RUN THE CODE//
** 
Run the command: 
>> python3 learn.py (where the name of the training file is training.txt)
It will generate the file "input.txt"

Now run perceplearn.py with the following parameters
>>python3 perceplearn.py input.txt MODEL

A model file will be generated named MODEL

Now run the classify.py

>> python3 classify.py MODEL <hw3.test.err.txt> hw3.output.txt


